//CONSTANTS
const weatherApiKey = "1c975d50f8b92877430555e69025b7b3";

//POSITION OPTIONS
let getPositionOptions = {
    enableHighAccuracy: false,
    timeout: 7000,
    maximumAge: 0
};

//ERROR
function error(err) {
    console.warn(`ERROR(${err.code}): ${err.message}`);
}

// Convert string to title case
function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

// Get current position of user
function getPosition() {
    return new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(function (position) {
            console.log(position);
            resolve(position);
        }, function (err) {
            error(err);
            reject(err);
        }, getPositionOptions);
    });
}

// Get current weather information
function getCurrentWeather(position) {
    return new Promise(async (resolve, reject) => {
        let url = `https://api.openweathermap.org/data/2.5/weather?lat=${position.coords.latitude}&lon=${position.coords.longitude}&units=metric&APPID=${weatherApiKey}`
        fetch(url).then(function (result) {
            return result.json();
        }).then(function (json) {
            console.log(json);
            resolve(json);
        });
    });
}

// Get 3 day forecast information
function getForecast(position) {
    return new Promise(async (resolve, reject) => {
        let url = `https://api.openweathermap.org/data/2.5/forecast?lat=${position.coords.latitude}&lon=${position.coords.longitude}&units=metric&APPID=${weatherApiKey}`
        fetch(url).then(function (result) {
            return result.json();
        }).then(function (json) {
            console.log(json);
            resolve(json);
        });
    });
}

// Total rainfall over 12 hours
function getDailyRainfall(forecast) {
    return new Promise((resolve, reject) => {
        let totalRain = 0;
        for (i = 0; i < 5; i++) {
            try {
                totalRain += forecast.list[i].rain["3h"];
            } catch (error) {
                console.log(error)
            }
        }
        resolve(totalRain);
    });
}

// Lowest temp for 12 hours
function getDailyLow(forecast) {
    return new Promise((resolve, reject) => {
        let dailyLow = Infinity;
        for (i = 0; i < 5; i++) {
            if (forecast.list[i].main.temp_min < dailyLow) {
                dailyLow = forecast.list[i].main.temp_min;
            }
        }
        resolve(dailyLow);
    });
}

// Highest temp for 12 hours
function getDailyHigh(forecast) {
    return new Promise((resolve, reject) => {
        let dailyHigh = -Infinity;
        for (i = 0; i < 5; i++) {
            if (forecast.list[i].main.temp_max > dailyHigh) {
                dailyHigh = forecast.list[i].main.temp_max;
            }
        }
        resolve(dailyHigh);
    });
}

// Max wind speed for 12 hours
function getMaxWindSpeed(forecast) {
    return new Promise((resolve, reject) => {
        let maxSpeed = -Infinity;
        for (i = 0; i < 5; i++) {
            if (forecast.list[i].wind.speed > maxSpeed) {
                maxSpeed = forecast.list[i].wind.speed;
            }
        }
        resolve(maxSpeed);
    });
}


// Use weather and forecast objects to update the UI
function updateUI(weather, forecast) {
    return new Promise(async (resolve, reject) => {
        let location = forecast.city.name;
        let description = forecast.list[0].weather[0].description;
        let currentTemp = forecast.list[0].main.temp;
        let maxTemp = await getDailyHigh(forecast);
        let minTemp = await getDailyLow(forecast);
        let windSpeed = await getMaxWindSpeed(forecast);
        let rain = await getDailyRainfall(forecast)
        let rainText = `Rainfall: ${Math.round(rain)} mm`
        let sunrise = new Date(weather.sys.sunrise * 1000);
        let sunset = new Date(weather.sys.sunset * 1000);
        let weatherIcon = forecast.list[0].weather[0].icon;

        let locationText = location;
        let descriptionText = toTitleCase(description);
        let currentTempText = `Current: ${Math.round(currentTemp)}&deg`;
        let maxTempText = `Max: ${Math.round(maxTemp)}&deg`;
        let minTempText = `Min: ${Math.round(minTemp)}&deg`;
        let windSpeedText = `Wind Speed: ${Math.round(windSpeed)} km/h`;
        let sunriseText = `${("0" + sunrise.getHours()).slice(-2)}:${("0" + sunrise.getMinutes()).slice(-2)}`;
        let sunsetText = `${("0" + sunset.getHours()).slice(-2)}:${("0" + sunset.getMinutes()).slice(-2)}`;

        let weatherImgSrc = `https://openweathermap.org/img/w/${weatherIcon}.png`

        document.getElementById('location').innerHTML = locationText;
        document.getElementById('description').innerHTML = descriptionText;
        document.getElementById('sunrise').innerHTML = sunriseText;
        document.getElementById('sunset').innerHTML = sunsetText;
        document.getElementById('current-temp').innerHTML = currentTempText;
        document.getElementById('max-temp').innerHTML = maxTempText;
        document.getElementById('min-temp').innerHTML = minTempText;
        document.getElementById('rain').innerHTML = rainText;
        document.getElementById('wind-speed').innerHTML = windSpeedText;

        document.getElementById('weatherIcon').src = weatherImgSrc;
        document.getElementById('sunriseIcon').src = 'https://openweathermap.org/img/w/01d.png';
        document.getElementById('sunsetIcon').src = 'https://openweathermap.org/img/w/01n.png';

        resolve();
    })
}

async function main() {
    let position = await getPosition();
    let weather = await getCurrentWeather(position);
    let forecast = await getForecast(position);
    await updateUI(weather, forecast);
    let page = document.getElementById('page-container').style.visibility = 'visible';
}


