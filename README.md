# Fitness

Simple app to aid in personal fitness, especially running.

Requirements and features to be added as and when.

Primary purpose of app is to gain experience with web development.

# Requirements
- Must be friendly for both desktop and mobile
- Fast to load and switch views
- Easy to naviagate

# Current Features

# To-Do

Weather
- Summary of today's weather for running

Home Screen
- Switch between various features of web app

Integrate with 3rd party fitness data APIs
- Strava, Google Fit, Garmin

